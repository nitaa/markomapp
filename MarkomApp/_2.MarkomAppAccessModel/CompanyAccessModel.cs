﻿using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace _2.MarkomAppAccessModel
{
    public class CompanyAccessModel
    {
        public static string Message;
        public static string DataKosong;
        public static List<CompanyViewModel> GetListAll(string searchName, string searchCode, DateTime?searchDate, string searchCreatedCompany)
        {
            List<CompanyViewModel> listData = new List<CompanyViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listData = (from c in db.m_company
                            where c.is_delete == false
                            select new CompanyViewModel
                            {
                                id = c.id,
                                code = c.code,
                                name = c.name,
                                phone = c.phone,
                                email = c.email,
                                address = c.address,
                                isDelete = c.is_delete,
                                createdBy = c.created_by,
                                createdDate = c.created_date,
                                updatedBy = c.updated_by,
                                updatedDate = c.updated_date
                            }).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                listData = listData.Where(x => x.name == searchName).ToList();
            }
            if (!string.IsNullOrEmpty(searchCode))
            {
                listData = listData.Where(x => x.code == searchCode).ToList();
            }
            if (searchDate != null)
            {
                listData = listData.Where(x => x.createdDate.ToString("dd MMMM yyyy") == searchDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (!string.IsNullOrEmpty(searchCreatedCompany))
            {
                listData = listData.Where(x => x.createdBy == searchCreatedCompany).ToList();
            }
            if (listData.Count == 0)
            {
                DataKosong = "Data tidak ditemukan";
            }
            else
            {
                DataKosong = "";
            }
            return listData;
        }

        public static CompanyViewModel GetDetailById(int Id)
        {
            CompanyViewModel result = new CompanyViewModel();
            using (var db = new DB_MarkomEntities())
            {
                result = (from c in db.m_company
                          where c.id == Id
                          select new CompanyViewModel
                          {
                              id = c.id,
                              code = c.code,
                              name = c.name,
                              phone = c.phone,
                              email = c.email,
                              address = c.address,
                              isDelete = c.is_delete,
                              createdBy = c.created_by,
                              createdDate = c.created_date,
                              updatedBy = c.updated_by,
                              updatedDate = c.updated_date

                          }).FirstOrDefault();
            }
            return result;
        }
        public static List<CompanyViewModel> CompanyCodeName()
        {
            List<CompanyViewModel> listNote = new List<CompanyViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listNote = (from c in db.m_company
                            where c.is_delete == false
                            select new CompanyViewModel
                            {
                                code = c.code,
                                name = c.name,
                                email = c.email,
                                phone = c.phone,
                                address = c.address,
                            }).ToList();
            }
            return listNote;
            
        }
        public static bool Insert(CompanyViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_company c = new m_company();
                    c.code = paramModel.code;
                    c.name = paramModel.name;
                    c.email = paramModel.email;
                    c.phone = paramModel.phone;
                    c.address = paramModel.address;
                    c.is_delete = paramModel.isDelete;
                    c.created_by = paramModel.createdBy;
                    c.created_date = paramModel.createdDate;

                    db.m_company.Add(c);
                    db.SaveChanges();
                }
                Message = "Data berhasil disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Update(CompanyViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_company c = db.m_company.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (c != null)
                    {
                        c.name = paramModel.name;
                        c.email = paramModel.email;
                        c.address = paramModel.address;
                        c.phone = paramModel.phone;
                        c.updated_by = paramModel.updatedBy;
                        c.updated_date = paramModel.updatedDate;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data tidak ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Delete(CompanyViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_company c = db.m_company.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (c != null)
                    {
                        c.is_delete = true;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

    }
    
}
