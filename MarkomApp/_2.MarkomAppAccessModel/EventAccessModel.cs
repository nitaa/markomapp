﻿using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.MarkomAppAccessModel
{
    public class EventAccessModel
    {
        public static string Message;
        public static string DataKosong;

        public static List<EventViewModel> GetListAll(string searchCode, string searchRequested, string searchStatus, DateTime? searchReqDate, DateTime ? searchCreatedDate, string searchCreatedEvent)
        {
            List<EventViewModel> listData = new List<EventViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listData = (from e in db.t_event
                            
                            where e.is_delete == false
                            select new EventViewModel
                            {
                                id = e.id,
                                code = e.code,
                                eventName = e.event_name,
                                startDate = e.start_date,
                                endDate = e.end_date,
                                place = e.place,
                                budget = e.budget,
                                requestBy = e.request_by,
                                requestDate = e.request_date,
                                //approvedDate = e.approved_date
                                //approvedDate = e.approved_date,
                                //assignTo = e.assign_to,
                                //closedDate = e.closed_date,
                                //note = e.note,
                                //status = e.status,
                                //rejectReason = e.reject_reason,
                                //isDelete = e.is_delete,
                                createdBy = e.created_by,
                                //createdDate = e.created_date,
                                //updatedBy = e.updated_by,
                                //updatedDate = e.updated_date
                            }).ToList();
            }
            if (!string.IsNullOrEmpty(searchCode))
            {
                listData = listData.Where(x => x.code == searchCode).ToList();
            }
            if (!string.IsNullOrEmpty(searchCreatedEvent))
            {
                listData = listData.Where(x => x.createdBy == searchCreatedEvent).ToList();
            }
            if (searchCreatedDate != null)
            {
                listData = listData.Where(x => x.createdDate.ToString("dd MMMM yyyy") == searchCreatedDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (searchReqDate != null)
            {
                listData = listData.Where(x => x.requestDate.ToString("dd MMMM yyyy") == searchReqDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (listData.Count == 0)
            {
                DataKosong = "Data tidak ditemukan";
            }
            else
            {
                DataKosong = "";
            }

            return listData;
        }

       
        //public static EventViewModel ByEmployee(int id)
        //{
        //    EventViewModel result = new EventViewModel();
        //    using (var db = new DB_MarkomEntities())
        //    {
        //        result = (from e in db.t_event
        //                  join m in db.m_employee
        //                  on e.request_by equals m.id
        //                  where e.is_delete == false
        //                  select new EventViewModel
        //                  && e. == id
        //                  {
        //                   id = e.id,
        //                   eventName = e.event_name,
        //                   rejectReason = e.reject_reason,
        //                   requestBy = e.request_by,
        //                   requestDate = e.request_date,
        //                   lastName = m.last_name,
        //                   firstName = m.first_name,
                           

        //                  }).FirstOrDefault();
        //    }
        //    return result;
        //}

        public static EventViewModel GetDetailById(int Id)
        {
            EventViewModel result = new EventViewModel();
            using (var db = new DB_MarkomEntities())
            {
                result = (from e in db.t_event
                          where e.id == Id
                          select new EventViewModel
                          {
                              id = e.id,
                              eventName = e.event_name,
                              rejectReason = e.reject_reason,
                              requestBy = e.request_by,
                              createdBy = e.created_by,
                              requestDate = e.request_date,
                              code = e.code,
                              endDate =e.end_date,
                              startDate =e.start_date,
                              budget = e.budget,
                             updatedBy = e.updated_by,
                            



                          }).FirstOrDefault();
            }
            
            return result;
            
        }
        public static List<EventViewModel>EventCodeName()
        {
            List<EventViewModel> listNote = new List<EventViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listNote = (from e in db.t_event
                            where e.is_delete == false
                            select new EventViewModel
                            {
                                code = e.code,
                                eventName = e.event_name
                            }).ToList();
            }
            return listNote;
        }

        public static bool Insert(EventViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    t_event e = new t_event();
                    e.code = paramModel.code;
                    e.event_name = paramModel.eventName;
                    e.status = paramModel.status;
                    e.is_delete = paramModel.isDelete;
                    e.start_date = paramModel.startDate;
                    e.closed_date = paramModel.closedDate;
                    e.end_date = paramModel.endDate;
                    e.reject_reason = paramModel.rejectReason;
                    e.request_by = paramModel.requestBy;
                    e.request_date = paramModel.requestDate;
                    e.created_by = paramModel.createdBy;
                    e.created_date = paramModel.createdDate;
                    e.approved_by = paramModel.approvedBy;
                    e.approved_date = paramModel.approvedDate;
                    e.note = paramModel.note;
                    e.place = paramModel.place;

                    db.t_event.Add(e);
                    db.SaveChanges();
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
    }
}
