﻿using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace _2.MarkomAppAccessModel
{
    public class UnitAccessModel
    {
        public static string DataKosong;
        public static string Message;

        public static List<UnitViewModel> GetListAll(string searchName, string searchCode, DateTime? searchDate, string searchCreatedUnit)
        {
            List<UnitViewModel> listData = new List<UnitViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listData = (from a in db.m_unit.Where(c => c.is_delete == false)
                            join b in db.m_souvenir.Select(o => new { o.m_unit_id}).Distinct()
                            on a.id equals b.m_unit_id into newgroup
                            from c in newgroup.DefaultIfEmpty()
                            where a.is_delete == false
                            select new UnitViewModel
                            {
                                id = a.id,
                                code = a.code,
                                name = a.name,
                                description = a.description,
                                isDelete = a.is_delete,
                                deleteunit = c.m_unit_id.ToString()==null ? null:c.m_unit_id.ToString(),
                                createdBy = a.created_by,
                                createdDate = a.created_date,
                                updatedBy = a.updated_by,
                                updatedDate = a.updated_date
                            }).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                listData = listData.Where(x => x.name == searchName).ToList();
            }
            if (!string.IsNullOrEmpty(searchCode))
            {
                listData = listData.Where(x => x.code == searchCode).ToList();
            }
            if (searchDate != null)
            {
                listData = listData.Where(x => x.createdDate.ToString("dd MMMM yyyy") == searchDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (!string.IsNullOrEmpty(searchCreatedUnit))
            {
                listData = listData.Where(x => x.createdBy == searchCreatedUnit).ToList();
            }
            if (listData.Count == 0)
            {
                DataKosong = "Data tidak ditemukan";
            }
            else
            {
                DataKosong = "";
            }

            return listData;
        }
        
        public static UnitViewModel GetDetailsById(int Id)
        {
            UnitViewModel result = new UnitViewModel();
            using (var db = new DB_MarkomEntities())
            {
                result = (from u in db.m_unit
                          where u.id == Id
                          select new UnitViewModel
                          {
                              id = u.id,
                              code = u.code,
                              name=u.name,
                              description=u.description,
                              isDelete=u.is_delete,
                              createdBy=u.created_by,
                              createdDate=u.created_date,
                              updatedBy=u.updated_by,
                              updatedDate=u.updated_date
                          }).FirstOrDefault();
            }
            return result;
        }

        public static List<UnitViewModel> UnitCodeName()
        {
            List<UnitViewModel> listNote = new List<UnitViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listNote = (from u in db.m_unit
                            where u.is_delete == false
                            select new UnitViewModel
                            {
                                code = u.code,
                                name = u.name
                            }).ToList();
            }
            return listNote;
        }

        public static bool Insert(UnitViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_unit u = new m_unit();
                    u.code = paramModel.code;
                    u.name = paramModel.name;
                    u.description = paramModel.description;
                    u.is_delete = paramModel.isDelete;
                    u.created_by = paramModel.createdBy;
                    u.created_date = paramModel.createdDate;

                    db.m_unit.Add(u);
                    db.SaveChanges();
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner Exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Update(UnitViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_unit u = db.m_unit.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (u != null)
                    {
                        u.name = paramModel.name;
                        u.description = paramModel.description;
                        u.updated_by = paramModel.updatedBy;
                        u.updated_date = paramModel.updatedDate;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data tidak ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
                
            }
            return result;
        }


        public static bool Delete(UnitViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_unit u = db.m_unit.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (u != null)
                    {
                        u.is_delete = true;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
                
            }
            return result;
        }
    }
}
