﻿using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace _2.MarkomAppAccessModel
{
    public class RoleAccessModel
    {
        public static string DataKosong;

        public static string Message;

        public static List<RoleViewModel> GetListAll(string searchName, string searchCode, DateTime? searchDate, string searchCreatedRole)
        {
            List<RoleViewModel> listData = new List<RoleViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listData = (from r in db.m_role
                            where r.is_delete == false
                            select new RoleViewModel
                            {
                                id = r.id,
                                code = r.code,
                                name = r.name,
                                description = r.description,
                                isDelete = r.is_delete,
                                createdBy = r.created_by,
                                createdDate=r.created_date,
                                updatedBy = r.updated_by,
                                updatedDate = r.updated_date
                            }).ToList();
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                listData = listData.Where(x => x.name == searchName).ToList();
            }
            if (!string.IsNullOrEmpty(searchCode))
            {
                listData = listData.Where(x => x.code == searchCode).ToList();
            }
            if (searchDate != null)
            {
                listData = listData.Where(x => x.createdDate.ToString("dd MMMM yyyy") == searchDate.Value.ToString("dd MMMM yy")).ToList();
            }
            if (!string.IsNullOrEmpty(searchCreatedRole))
            {
                listData = listData.Where(x => x.createdBy == searchCreatedRole).ToList();

            }
            else
            {
                DataKosong = "";
            }
            return listData;
        }

        public static RoleViewModel GetDetailsById(int Id)
        {
            RoleViewModel result = new RoleViewModel();
            using (var db = new DB_MarkomEntities())
            {
                result = (from r in db.m_role
                          where r.id == Id
                          select new RoleViewModel
                          {
                              id = r.id,
                              code = r.code,
                              name = r.name,
                              description = r.description,
                              isDelete = r.is_delete,
                              createdBy = r.created_by,
                              createdDate = r.created_date,
                              updatedBy = r.updated_by,
                              updatedDate = r.updated_date
                          }).FirstOrDefault();
            }
            return result;
        }

        public static List<RoleViewModel> RoleCodeName()
        {
            List<RoleViewModel> listNote = new List<RoleViewModel>();
            using (var db = new DB_MarkomEntities())
            {
                listNote = (from r in db.m_role
                            where r.is_delete == false
                            select new RoleViewModel
                            {
                                code = r.code,
                                name = r.name
                            }).ToList();
            }
            return listNote;
        }


        public static bool Insert(RoleViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_role r = new m_role();
                    r.code = paramModel.code;
                    r.name = paramModel.name;
                    r.description = paramModel.description;
                    
                    r.is_delete = paramModel.isDelete;
                    r.created_by = paramModel.createdBy;
                    r.created_date = paramModel.createdDate;

                    db.m_role.Add(r);
                    db.SaveChanges();
                }
                Message = "Data berhasil disimpan";
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Update(RoleViewModel paramModel)
        {
            bool result= true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_role r = db.m_role.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (r != null)
                    {
                        r.name = paramModel.name;
                        r.description = paramModel.description;
                        r.updated_by = paramModel.updatedBy;
                        r.updated_date = paramModel.updatedDate;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data tidak ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            catch (Exception hasError)
            
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner Exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
                
            }
            return result;
        }

        public static bool Delete(RoleViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new DB_MarkomEntities())
                {
                    m_role r = db.m_role.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (r != null)
                    {
                        r.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
                Message = "Data Berhasil Disimpan";
            }
            
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("Inner Exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
    }
}
