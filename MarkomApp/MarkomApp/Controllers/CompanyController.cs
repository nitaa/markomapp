﻿using _2.MarkomAppAccessModel;
using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace MarkomApp.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string searchName, string searchCode, DateTime? searchDate, string searchCreatedCompany)
        {
            List<CompanyViewModel> List = new List<CompanyViewModel>();
            List = CompanyAccessModel.GetListAll(searchName, searchCode, searchDate, searchCreatedCompany);
            ViewBag.CompanyCode = new SelectList(List, "code", "code");
            ViewBag.CompanyName = new SelectList(List, "name", "name");
            ViewBag.DataKosong = CompanyAccessModel.DataKosong;
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(CompanyViewModel paramModel)
        {
            try
            {
                CompanyAccessModel.Message = string.Empty;

                //Create auto generate
                using (var db = new DB_MarkomEntities())
                {
                    string nol = "";
                    m_company cek = db.m_company.OrderByDescending(x => x.code).First();
                    int simpan = int.Parse(cek.code.Substring(3));
                    simpan++;
                    for (int i = simpan.ToString().Length; i < 4; i++)
                    {
                        nol = nol + "0";

                    }
                    paramModel.code = "CP" + nol + simpan;
                }
                paramModel.createdBy = "Administrator";
                paramModel.createdDate = DateTime.Now;
                paramModel.isDelete = false;

                if (null == paramModel.name)
                {
                    CompanyAccessModel.Message = "Anda belum mengisi semua data. silahkan ulangi kembali";
                }
                if (string.IsNullOrEmpty(CompanyAccessModel.Message))
                {
                    return Json(new
                    {
                        success = CompanyAccessModel.Insert(paramModel),
                        message = paramModel.code
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = CompanyAccessModel.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(Int32 paramId)
        {
            return PartialView(CompanyAccessModel.GetDetailById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(CompanyViewModel paramModel)
        {
            try
            {
                CompanyAccessModel.Message = string.Empty;

                paramModel.updatedBy = "Nita";
                paramModel.updatedDate = DateTime.Now;
                if (null == paramModel.name)
                {
                    CompanyAccessModel.Message = "Anda belum mengisi semua data, silahkan ulangi kembali";
                }
                if (string.IsNullOrEmpty(CompanyAccessModel.Message))
                {
                    return Json(new
                    {
                        success = CompanyAccessModel.Update(paramModel),
                        message = CompanyAccessModel.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = CompanyAccessModel.Message },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                   JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(CompanyAccessModel.GetDetailById(paramId));
        }
        public ActionResult Delete(Int32 paramId)
        {
            return PartialView(CompanyAccessModel.GetDetailById(paramId));
        }
        [HttpPost]
        public ActionResult Delete(CompanyViewModel paramModel)
        {
            try
            {
                return Json(new
                {
                    success = CompanyAccessModel.Delete(paramModel),
                    message = CompanyAccessModel.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                   JsonRequestBehavior.AllowGet);
            }
        }

    }
}