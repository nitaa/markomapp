﻿using _2.MarkomAppAccessModel;
using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarkomApp.Controllers
{
    public class EventController : Controller
    {
        // GET: Event
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(string searchCode, string searchRequested, string searchStatus, DateTime? searchReqDate, DateTime? searchCreatedDate, string searchCreatedEvent)
        {
            List<EventViewModel> List = new List<EventViewModel>();
            List = EventAccessModel.GetListAll(searchCode, searchRequested, searchStatus, searchReqDate, searchCreatedDate, searchCreatedEvent);
            ViewBag.EventCode = new SelectList(List, "code", "code");
            ViewBag.DataKosong = EventAccessModel.DataKosong;
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(EventViewModel paramModel)
        {
            try
            {
                EventAccessModel.Message = string.Empty;

                //Create auto generate unit_code
                using (var db = new DB_MarkomEntities())
                {
                    string nol = "";
                    t_event cek = db.t_event.OrderByDescending(x => x.code).First();
                    int simpan = int.Parse(cek.code.Substring(3));
                    simpan++;
                    for (int i = simpan.ToString().Length; i < 5; i++)
                    {
                        nol = nol + "0";
                    }
                    paramModel.code = "TRWOEV300318" + nol + simpan;
                }
                paramModel.requestBy = 1;
                paramModel.requestDate = DateTime.Now;
                paramModel.isDelete = false;
                if (null == paramModel.eventName)
                {
                    EventAccessModel.Message = "Anda belum memasukan semua data. Silahkan ulangi kembali";
                }
                if (string.IsNullOrEmpty(EventAccessModel.Message))
                {
                    return Json(new
                    {
                        success = EventAccessModel.Insert(paramModel),
                        message = paramModel.code
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = EventAccessModel.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                   JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Detail (Int32 paramId)
        {
            return PartialView(EventAccessModel.GetDetailById(paramId));
        }
    }
}