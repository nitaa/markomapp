﻿using _2.MarkomAppAccessModel;
using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarkomApp.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string searchName, string searchCode, DateTime? searchDate, string searchCreatedRole)
        {
            List<RoleViewModel> List = new List<RoleViewModel>();
            List = RoleAccessModel.GetListAll(searchName, searchCode, searchDate, searchCreatedRole);
            ViewBag.RoleCode = new SelectList(List, "code", "code");
            ViewBag.RoleName = new SelectList(List, "name", "name");
            ViewBag.DataKosong = RoleAccessModel.DataKosong;
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(RoleViewModel paramModel)
        {
            try
            {
                RoleAccessModel.Message = string.Empty;

                //Create auto generate
                using (var db = new DB_MarkomEntities())
                {
                    string nol = "";
                    m_role cek = db.m_role.OrderByDescending(x => x.code).First();
                    int simpan = int.Parse(cek.code.Substring(3));
                    simpan++;
                    for (int i = simpan.ToString().Length; i < 4; i++)
                    {
                        nol = nol + "0";

                    }
                    paramModel.code = "RO" + nol + simpan;
                }
                paramModel.createdBy = "Administrator";
                paramModel.createdDate = DateTime.Now;
                paramModel.isDelete = false;

                if (null == paramModel.name)
                {
                    RoleAccessModel.Message = "Anda belum mengisi semua data. silahkan ulangi kembali";
                }
                if (string.IsNullOrEmpty(RoleAccessModel.Message))
                {
                    return Json(new
                    {
                        success = RoleAccessModel.Insert(paramModel),
                        message = paramModel.code
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = RoleAccessModel.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Edit(Int32 paramId)
        {
            return PartialView(RoleAccessModel.GetDetailsById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(RoleViewModel paramModel)
        {
            try
            {
                RoleAccessModel.Message = string.Empty;

                paramModel.updatedBy = "Nita";
                paramModel.updatedDate = DateTime.Now;

                if (null == paramModel.name)
                {
                    RoleAccessModel.Message = "Anda belum memasukkan semua data, silahkan diulangi kembali";

                }
                if (string.IsNullOrEmpty(RoleAccessModel.Message))
                {
                    return Json(new
                    {
                        success = RoleAccessModel.Update(paramModel),
                        message = RoleAccessModel.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = RoleAccessModel.Message },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
                
            }
        }

        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(RoleAccessModel.GetDetailsById(paramId));
        }

        public ActionResult Delete(Int32 paramId)
        {
            return PartialView(RoleAccessModel.GetDetailsById(paramId));
        }
        [HttpPost]
        public ActionResult Delete(RoleViewModel paramModel)
        {
            try
            {
                return Json(new
                {
                    success = RoleAccessModel.Delete(paramModel),
                    message = RoleAccessModel.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }

        }
    }  

}