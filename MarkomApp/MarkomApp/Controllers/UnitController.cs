﻿using _2.MarkomAppAccessModel;
using _3.MarkomAppViewModel;
using _4.MarkomAppDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace MarkomApp.Controllers
{
    public class UnitController : Controller
    {
        // GET: Unit
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(string searchName, string searchCode, DateTime? searchDate, string searchCreatedUnit )
        {
            List<UnitViewModel> List = new List<UnitViewModel>();
            List = UnitAccessModel.GetListAll(searchName, searchCode, searchDate, searchCreatedUnit);
            //List = UnitAccessModel.GetListAll(searchName, searchDate, searchCode, searchCreatedUnit);
            ViewBag.UnitCode = new SelectList(List, "code", "code");
            ViewBag.UnitName = new SelectList(List, "name", "name");
            ViewBag.DataKosong = UnitAccessModel.DataKosong;
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(UnitViewModel paramModel)
        {
            try
            {
                UnitAccessModel.Message = string.Empty;

                //auto generate code
                using (var db = new DB_MarkomEntities())
                {
                    string nol = "";
                    m_unit cek = db.m_unit.OrderByDescending(x => x.code).First();
                    int simpan = int.Parse(cek.code.Substring(3));
                    simpan++;
                    for (int i = simpan.ToString().Length; i < 4; i++)
                    {
                        nol = nol + "0";
                    }
                    paramModel.code = "UN" + nol + simpan;
                }
                paramModel.createdBy = "Administrator";
                paramModel.createdDate = DateTime.Now;
                paramModel.isDelete = false;

                if (null == paramModel.name)
                {
                    UnitAccessModel.Message = "Anda harus mengisi semua data";
                }
                if (string.IsNullOrEmpty(UnitAccessModel.Message))
                {
                    return Json(new
                    {
                        success = UnitAccessModel.Insert(paramModel),
                        message = paramModel.code
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = UnitAccessModel.Message },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
                {
                return Json(new { success = false, message = hasError.Message},
                    JsonRequestBehavior.AllowGet);
                }
            }
        public ActionResult Edit(Int32 paramId)
        {
            return PartialView(UnitAccessModel.GetDetailsById(paramId));
        }
        [HttpPost]
        public ActionResult Edit(UnitViewModel paramModel)
        {
            try
            {
                UnitAccessModel.Message = string.Empty;

                paramModel.updatedBy = "Nita";
                paramModel.updatedDate = DateTime.Now;

                if (null == paramModel.name)
                {
                    UnitAccessModel.Message = "Silahkan isi semua data terlebih dahulu";

                }
                if (string.IsNullOrEmpty(UnitAccessModel.Message))
                {
                    return Json(new
                    {
                        success = UnitAccessModel.Update(paramModel),
                        message = UnitAccessModel.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = UnitAccessModel.Message },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(UnitAccessModel.GetDetailsById(paramId));
        }
        public ActionResult Delete(Int32 paramId)
        {
            return PartialView(UnitAccessModel.GetDetailsById(paramId));
        }
        [HttpPost]
        public ActionResult Delete(UnitViewModel paramModel)
        {
            try
            {
                return Json(new
                {
                    success = UnitAccessModel.Delete(paramModel),
                    Message = UnitAccessModel.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
                throw;
            }
        }


        }
    }
