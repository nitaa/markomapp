﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.MarkomAppViewModel
{
    public class EventViewModel
    {
        public int id { get; set; }
        public string code { get; set; }
        public string eventName { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public string place { get; set; }
        public Nullable<decimal> budget { get; set; }
        public int requestBy { get; set; }
        public System.DateTime requestDate { get; set; }
        public int approvedBy { get; set; }
        public System.DateTime approvedDate { get; set; }
        public int assignTo { get; set; }
        public System.DateTime closedDate { get; set; }
        public string note { get; set; }
        public int status { get; set; }
        public string rejectReason { get; set; }
        public bool isDelete { get; set; }
        public string createdBy { get; set; }
        public System.DateTime createdDate { get; set; }
        public string updatedBy { get; set; }
        public System.DateTime updatedDate { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string idE { get; set; }
    }
}
