﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.MarkomAppViewModel
{
    public class CompanyViewModel
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public bool isDelete { get; set; }
        public string createdBy { get; set; }
        public System.DateTime createdDate { get; set; }
        public string tanggal => createdDate.ToString("dd MMMM yyyy");
        
        public string updatedBy { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
    }
}
